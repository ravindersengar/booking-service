FROM openjdk:8-jdk-alpine
EXPOSE 4003
VOLUME /main-app
ADD /target/user-service-0.1.0.jar user-service-0.1.0.jar
ENTRYPOINT ["java","-jar","user-service-0.1.0.jar"]
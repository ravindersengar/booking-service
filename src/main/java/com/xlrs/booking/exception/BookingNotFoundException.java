package com.xlrs.booking.exception;

import com.xlrs.commons.exception.ValidationException;

public final class BookingNotFoundException extends RuntimeException implements ValidationException{

    private static final long serialVersionUID = 5861310537366287163L;

    public BookingNotFoundException() {
        super();
    }

    public BookingNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public BookingNotFoundException(final String message) {
        super(message);
    }

    public BookingNotFoundException(final Throwable cause) {
        super(cause);
    }

}

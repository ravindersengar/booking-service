package com.xlrs.booking.service;

import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.booking.view.BookingView;

public interface BookingService {

	public BookingView createBooking(BookingView bookingView) throws NoResultFoundException;
	
	public BookingView getBooking(Long bookingId) throws NoResultFoundException;

}

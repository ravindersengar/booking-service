package com.xlrs.booking.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.booking.entity.Booking;
import com.xlrs.booking.repository.BookingRepository;
import com.xlrs.booking.view.BookingView;

@Service
public class BookingServiceImpl implements BookingService {

	@Autowired
	private MessageSource messages;
	
	@Autowired
	private BookingRepository BookingRepository;
	
	@Autowired
	private BaseRepository<Booking> baseRepository;

	@Override
	public BookingView createBooking(BookingView BookingView) throws NoResultFoundException {
		Booking Booking = BookingView.getId() != null ? getBookingById(BookingView.getId()) : new Booking();
		Booking.setName(BookingView.getName());
		Booking.setId(BookingView.getId());
		Booking.setSsn(BookingView.getSsn());
		
		Booking = BookingRepository.save(baseRepository.addAuditFeilds(Booking, null, CommonConstants.STATUS_ACTIVE));
		return new BookingView(Booking.getId(), Booking.getName(), Booking.getSsn());
	}

	@Override
	public BookingView getBooking(Long id) throws NoResultFoundException  {
		Booking Booking = getBookingById(id);
		return new BookingView(Booking.getId(), Booking.getName(), Booking.getSsn());
	}

	public Booking getBookingById(Long id) throws NoResultFoundException {
		Optional<Booking> optional = BookingRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		Booking Booking = optional.get();
		return Booking;
	}

}

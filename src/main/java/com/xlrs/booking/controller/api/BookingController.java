package com.xlrs.booking.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.booking.service.BookingService;
import com.xlrs.booking.view.BookingView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/booking")
@Slf4j
public class BookingController {

	@Autowired
	private BookingService bookingService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createBooking(@RequestBody BookingView bookingView) throws Exception {
		log.debug("Requested payload is : " + bookingView);
		
		try {
			return new ResponseView(bookingService.createBooking(bookingView));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseView getBooking(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(bookingService.getBooking(id));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
}
